import requests
import json
import csv

request = requests.get("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5").text
a = json.loads(request)

currency_buy = []
currency_sale = []

for zminna in a:
    if zminna['ccy'] == 'USD' and zminna['base_ccy'] == 'UAH':
        currency_buy = float(zminna.get('buy'))
        currency_sale = float(zminna.get('sale'))

currencyUSD = []
currencyUAH = []
with open('currency.csv', 'r') as csvfile:
    column_names = ['Currency', 'Amount']
    ls = csv.DictReader(csvfile)
    for row in ls:
        if row['Currency'] == 'USD':
            currencyUSD.append(float(row['Amount']))
        elif row['Currency'] == 'UAH':
            currencyUAH.append(float(row['Amount']))

def check_money(user_wish):
    if user_wish == 'COURSE USD':
        print(100 * '-')
        print(f'COURSE USD\n RATE: "{currency_buy}", AVAILABLE:"{currencyUSD[-1]}"')
    elif user_wish == 'COURSE UAH':
        print(100 * '-')
        print(f'COURSE UAH\n RATE: "{currency_sale}", AVAILABLE: "{currencyUAH[-1]}"')
    else:
        print(100 * '-')
        print('INVALID CURRENCY')

def buy(input_sum1):

    if input_sum1 <= currencyUSD[-1]:
        buy_dollar = input_sum1 * currency_sale
        print('You need to give - ', buy_dollar, 'UAH')
        ch_USD1 = currencyUSD[-1] - input_sum1
        ch_UAH1 = currencyUAH[-1] + buy_dollar

        with open('currency.csv', 'r+') as csvfile:
            lines = csvfile.readlines()
            lines[1] = 'USD,' + str(ch_USD1) + '\n'
            file_writer = csv.writer(csvfile, delimiter=',')
            file_writer.writerow(['USD', str(ch_USD1)])
            file_writer.writerow(['UAH', str(ch_UAH1)])
    else:
        print(f'UNAVAILABLE, REQUIRED BALANCE USD "{input_sum1}", AVAILABLE "{currencyUSD[-1]}"')

def sale(input_sum2):

    if input_sum2 <= currencyUAH[-1]:
        sale_dollar = input_sum2 * currency_buy
        print('You will get - ', sale_dollar, 'UAH')
        ch_USD2 = currencyUSD[-1] + input_sum2
        ch_UAH2 = currencyUAH[-1] - sale_dollar

        with open('currency.csv', 'r+') as csvfile:
            lines = csvfile.readlines()
            lines[2] = 'UAH,' + str(ch_USD2) + '\n'
            file_writer = csv.writer(csvfile, delimiter=',')
            file_writer.writerow(['USD', str(ch_USD2)])
            file_writer.writerow(['UAH', str(ch_UAH2)])
    else:
        print(f'UNAVAILABLE, REQUIRED BALANCE USD "{input_sum2}", AVAILABLE "{currencyUAH[-1]}"')
