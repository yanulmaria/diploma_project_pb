from def_helper import buy, sale, check_money

while True:
    print('COURSE USD/UAH')
    print('EXCHANGE USD')
    print('EXCHANGE UAH')
    print('STOP')
    print(100 * '-')

    client_choose = input('Your choice - ')
    input_split = client_choose.split()

    if len(input_split) == 1:
        print('Your session has ended')
        print(100 * '-')
        break

    elif len(input_split) == 2:
        user_wish = client_choose
        check_money(user_wish)
        print(100 * '-')

    elif len(input_split) == 3:

        if input_split[0] == 'EXCHANGE' and input_split[1] == 'USD':
            input_sum1 = float(input_split[2])
            buy(input_sum1)
        print(100 * '-')

        if input_split[0] == 'EXCHANGE' and input_split[1] == 'UAH':
            input_sum2 = float(input_split[2])
            sale(input_sum2)
        print(100 * '-')
    else:
        print('Please, make your choice')
        print(100 * '-')
